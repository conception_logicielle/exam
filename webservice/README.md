# Webservice

## Quickstart

- Create a virtual environnement : `python3 -m venv venvWebservice`
- activate it : `venvWebservice/bin/activate`
- install dependencies : `pip install -r requirements.txt`
- run the http client : `uvicorn main:app`
from fastapi import FastAPI

import requests

app = FastAPI()


decks_id = []


@app.get("/")
def read_root():
    result = "deck of cards"
    return result.upper()


@app.get("/creer-un-deck")
def get_id_deck():
    requete_id = requests.get(
        "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    response_id = requete_id.json()

    result = response_id["deck_id"]
    decks_id.append(result)
    return result


@app.post("/cartes/{nombre_cartes}")
def new_deck(deck: dict):
    return deck


@app.get("/cartes/{nombre_cartes}")
def get_cards(nombre_cartes: int):
    if len(decks_id) == 0:
        return("creer d'abord un deck")
    requete_cards = requests.get(
        "https://deckofcardsapi.com/api/deck/"+decks_id[-1]+"/draw/?count="+str(nombre_cartes))
    response_cards = requete_cards.json()

    result = {"deck_id": decks_id[-1],
              "cards": response_cards["cards"]}

    return result

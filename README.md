# Application Deck of Cards

Il s'agit d'une application permettant de requeter l'API [deckofcards](https://deckofcardsapi.com/) pour créer un webservice et un client requetant ce webservice.

# Quickstart

- lancer le webservice : `cd exam/webservice` et puis suivez les instructions du Quickstart
-  lancer le client http : `cd exam/client` et puis suivez les instructions du Quickstart
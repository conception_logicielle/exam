from main import count_cards_color


def test_count_cards_color():
    liste = [
        {
            "image": "https://deckofcardsapi.com/static/img/8C.png",
            "value": "8",
            "suit": "CLUBS",
            "code": "8C"
        },
        {
            "image": "https://deckofcardsapi.com/static/img/8D.png",
            "value": "8",
            "suit": "DIAMONDS",
            "code": "8D"
        },
        {
            "image": "https://deckofcardsapi.com/static/img/8C.png",
            "value": "8",
            "suit": "CLUBS",
            "code": "8C"
        }
    ]
    expected_res = {"C": 2, "D": 1}
    assert count_cards_color(liste) == expected_res

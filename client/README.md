# Client HTTP

Client HTTP permettant de requeter le webservice créé

## Quickstart

- Create a virtual environnement : `python3 -m venv venvClient`
- Activate it : `venvClient/bin/activate`
- Install dependencies : `pip install -r requirements.txt`
- Run the http client : `python3 main.py`
- Pour lancer le test unitaire : `pytest`
import requests


def count_cards_color(cards):
    colors = []
    for card in cards:
        color = card["code"][-1]
        colors.append(color)
    result = {i: colors.count(i) for i in set(colors)}
    return result


if __name__ == "__main__":

    deck_id = requests.get("http://localhost:8000/creer-un-deck")
    print(deck_id.status_code)

    deck = {"deck_id": "new_deck"}
    new_deck = requests.post("http://localhost:8000/cartes/10", json=deck)
    print(new_deck.status_code)

    cards_draw = requests.get("http://localhost:8000/cartes/10")
    print(cards_draw.status_code)

    dico_colors = count_cards_color(cards_draw.json()["cards"])
    print(dico_colors)
